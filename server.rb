require 'sinatra'
require './record'
require './result'
require 'json'
#######################   CONSTANTS AND INPUTS   ##############################
I_ARRIVAL_TIMES = [1, 2, 3, 4]
I_ARRIVAL_TIMES_PROBABILITIES = [0.25, 0.40, 0.20, 0.15]
ABLE_SERVICE_TIME = [10,12,14,16]
ABLE_SERVICE_TIME_PROBABILITY = [0.30, 0.28, 0.25, 0.17]
BAKER_SERVICE_TIME = [1,2,3,4]
BAKER_SERVICE_TIME_PROBABILITY = [0.35, 0.25, 0.20, 0.20]
NO_OF_CALLERS = 100
NO_OF_SIMULATIONS = 1000
###############################################################################

get '/' do
  erb :parameters
end

post '/single' do
  @post = params[:post]
  case @post[:interarrival]
    when '1'
      I_ARRIVAL_TIMES_PROBABILITIES = [0.25, 0.25, 0.25, 0.25]
    when '2'
      I_ARRIVAL_TIMES_PROBABILITIES = [0.07, 0.13, 0.26, 0.54]
    when '3'
      I_ARRIVAL_TIMES_PROBABILITIES = [0.17, 0.33, 0.33, 0.17]
    when '4'
      I_ARRIVAL_TIMES_PROBABILITIES = [0.25, 0.25, 0.25, 0.25]
    else
  end
  case @post[:able]
    when '1'
      ABLE_SERVICE_TIME_PROBABILITY = [0.25, 0.25, 0.25, 0.25]
    when '2'
      ABLE_SERVICE_TIME_PROBABILITY = [0.07, 0.13, 0.26, 0.54]
    when '3'
      ABLE_SERVICE_TIME_PROBABILITY = [0.17, 0.33, 0.33, 0.17]
    when '4'
      ABLE_SERVICE_TIME_PROBABILITY = [0.30, 0.28, 0.25, 0.17]
    else
  end
  case @post[:baker]
    when '1'
      BAKER_SERVICE_TIME_PROBABILITY = [0.25, 0.25, 0.25, 0.25]
    when '2'
      BAKER_SERVICE_TIME_PROBABILITY = [0.07, 0.13, 0.26, 0.54]
    when '3'
      BAKER_SERVICE_TIME_PROBABILITY = [0.17, 0.33, 0.33, 0.17]
    when '4'
      BAKER_SERVICE_TIME_PROBABILITY = [0.35, 0.25, 0.20, 0.20]
    else
  end
  unless @post[:callers] == ''
    NO_OF_CALLERS = @post[:callers].to_i
  end
  unless @post[:sims] == ''
    NO_OF_SIMULATIONS = @post[:sims].to_i
  end
  @interarrival_table = generate_inter_arrival_table
  @able_table = generate_able_table
  @baker_table = generate_baker_table
  @result = simulate(@interarrival_table, @able_table, @baker_table)
  @chart1 = @result.caller_delay.to_json
  @chart2 = @result.time_in_sys.to_json
  @chart3 = @result.time_population.to_json

  erb :simulation_results
end

post '/multiple' do
  @post = params[:post]
  case @post[:interarrival]
    when '2'
      I_ARRIVAL_TIMES_PROBABILITIES = [0.25, 0.25, 0.25, 0.25]
    when '3'
      I_ARRIVAL_TIMES_PROBABILITIES = [1, 0, 0, 0]
    when '4'
      I_ARRIVAL_TIMES_PROBABILITIES = [0.25, 0.25, 0.25, 0.25]
    else
  end
  case @post[:able]
    when '2'
      ABLE_SERVICE_TIME_PROBABILITY = [0.30, 0.28, 0.25, 0.17]
    when '3'
      ABLE_SERVICE_TIME_PROBABILITY = [0.30, 0.28, 0.25, 0.17]
    when '4'
      ABLE_SERVICE_TIME_PROBABILITY = [0.30, 0.28, 0.25, 0.17]
    else
  end
  case @post[:baker]
    when '2'
      BAKER_SERVICE_TIME_PROBABILITY = [0.35, 0.25, 0.20, 0.20]
    when '3'
      BAKER_SERVICE_TIME_PROBABILITY = [0.35, 0.25, 0.20, 0.20]
    when '4'
      BAKER_SERVICE_TIME_PROBABILITY = [0.35, 0.25, 0.20, 0.20]
    else
  end
  @interarrival_table = generate_inter_arrival_table
  @able_table = generate_able_table
  @baker_table = generate_baker_table
  multiple_simulation_results = Array.new
  NO_OF_SIMULATIONS.times do |sim|
    multiple_simulation_results << simulate(@interarrival_table, @able_table, @baker_table)
  end
  average_delay = Hash.new
  average_in_system = Hash.new
  multiple_simulation_results.each do |result|
    s = 0
    result.caller_delay.each do |k,v|
      s += (k.to_i * v)
    end
    average_delay.has_key?((s/NO_OF_CALLERS).round.to_s) ? average_delay[(s/NO_OF_CALLERS).round.to_s] +=1  : average_delay.store((s/NO_OF_CALLERS).round.to_s, 1)
    s2 = 0
    result.time_in_sys.each do |k,v|
      s2 += (k.to_i * v)
    end
    average_in_system.has_key?((s2/NO_OF_CALLERS).round.to_s) ? average_in_system[(s2/NO_OF_CALLERS).round.to_s] +=1  : average_in_system.store((s2/NO_OF_CALLERS).round.to_s, 1)
  end
  @delay_chart_data = average_delay.to_json
  @in_system_chart_data = average_in_system.to_json
  erb :multiple_simulations
end

def generate_inter_arrival_table
  table = Array.new
  cumulative = Array.new
  I_ARRIVAL_TIMES.each_index do |i|
    if cumulative.empty?
      cumulative << I_ARRIVAL_TIMES_PROBABILITIES[i]
      temp = (0..(100*I_ARRIVAL_TIMES_PROBABILITIES[i]).to_i-1)
    else
      cumulative << I_ARRIVAL_TIMES_PROBABILITIES[i] + cumulative[i-1]
      temp = ((100*cumulative[i-1]).to_i..(100*cumulative[i]).to_i-1)
    end
    table << [I_ARRIVAL_TIMES[i], I_ARRIVAL_TIMES_PROBABILITIES[i], cumulative[i], temp]
  end
  table
end

def generate_able_table
  table = Array.new
  cumulative = Array.new
  ABLE_SERVICE_TIME.each_index do |i|
    if cumulative.empty?
      cumulative << ABLE_SERVICE_TIME_PROBABILITY[i]
      temp = (0..(100*ABLE_SERVICE_TIME_PROBABILITY[i]).to_i-1)
    else
      cumulative << ABLE_SERVICE_TIME_PROBABILITY[i] + cumulative[i-1]
      temp = ((100*cumulative[i-1]).to_i..(100*cumulative[i]).to_i-1)
    end
    table << [ABLE_SERVICE_TIME[i], ABLE_SERVICE_TIME_PROBABILITY[i], cumulative[i], temp]
  end
  table
end

def generate_baker_table
  table = Array.new
  cumulative = Array.new
  BAKER_SERVICE_TIME.each_index do |i|
    if cumulative.empty?
      cumulative << BAKER_SERVICE_TIME_PROBABILITY[i]
      temp = (0..(100*BAKER_SERVICE_TIME_PROBABILITY[i]).to_i-1)
    else
      cumulative << BAKER_SERVICE_TIME_PROBABILITY[i] + cumulative[i-1]
      temp = ((100*cumulative[i-1]).to_i..(100*cumulative[i]).to_i-1)
    end
    table << [BAKER_SERVICE_TIME[i], BAKER_SERVICE_TIME_PROBABILITY[i], cumulative[i], temp]
  end
  table
end

def interarrival_time(table)
  r = rand(100)
  table.each do |row|
    return row[0] if row[3].include? r
  end
end

def service_time(table)
  r = rand(100)
  table.each do |row|
    #logger.info "row3:" + row[3].to_s
    if row[3].include? r
      return row[0]
    end
  end
end

def simulate(i,a,b)
  interarrival_table = i
  able_table = a
  #logger.info interarrival_table
  baker_table = b
  simulation_results = Array.new
  delays = Hash.new
  in_system = Hash.new
  time_population = Hash.new
  tkminus = 0
  when_able_available = 0
  when_baker_available = 0
  NO_OF_CALLERS.times do |caller|
    ak = interarrival_time(interarrival_table)
    tk = tkminus + ak
    server_chosen = ''
    service_begins = 0
    service_t = 0
    t_serv_completion_able = 0
    t_serv_completion_baker = 0
    twait = 0
    tsys = 0
    if tk >= when_able_available
      server_chosen = 'able'
      service_begins = tk
      service_t = service_time(able_table)
      #logger.warn "servt: " + service_t.to_s
      t_serv_completion_able = service_begins + service_t
      tsys = t_serv_completion_able - tk
    else
      if tk >= when_baker_available
        server_chosen = 'baker'
        service_begins = tk
        service_t = service_time(baker_table)
        t_serv_completion_baker = service_begins + service_t
        tsys = t_serv_completion_baker - tk
      else
        if when_able_available <= when_baker_available
          server_chosen = 'able'
          service_begins = when_able_available
          service_t = service_time(able_table)
          t_serv_completion_able = service_begins + service_t
          tsys = t_serv_completion_able - tk
          twait = when_able_available - tk
        else
          server_chosen = 'baker'
          service_begins = when_baker_available
          service_t = service_time(baker_table)
          t_serv_completion_baker = service_begins + service_t
          tsys = t_serv_completion_baker - tk
          twait = when_baker_available - tk
        end
      end
    end
    simulation_results << Record.new(caller+1, ak, tk, when_able_available, when_baker_available, server_chosen, service_t,
                                     service_begins, t_serv_completion_able, t_serv_completion_baker, twait, tsys)
    delays.has_key?(twait.to_s) ? delays[twait.to_s] +=1 : delays.store(twait.to_s, 1)
    in_system.has_key?(tsys.to_s) ? in_system[tsys.to_s] +=1 : in_system.store(tsys.to_s, 1)
    (simulation_results.last.arrival_time..simulation_results.last.arrival_time + simulation_results.last.time_in_sys).each do |i|
      time_population.has_key?(i) ? time_population[i] +=1 : time_population.store(i, 1)
    end
    @record = simulation_results.last
    if @record.server_chosen == 'able'
      when_able_available = @record.able_svc_completes
      when_baker_available = @record.when_baker_avail
    else
      when_baker_available = @record.baker_svc_completes
      when_able_available = @record.when_able_avail
    end
    # logger.warn "able:" + when_able_available.to_s
    # logger.warn "baker:" + when_baker_available.to_s
    tkminus = @record.arrival_time
  end
  no_delay_count = delays['0']
  delay_free_percentage = no_delay_count.to_f / NO_OF_CALLERS
  max_delay_percent = 0.0
  max_pop_delay = ''
  delays.each do |key, value|
    if key != '0' && max_delay_percent < value
      max_delay_percent = value
      max_pop_delay = key
    end
  end
  max_delay_percent = max_delay_percent.to_f / NO_OF_CALLERS

  percentages = [delay_free_percentage, max_delay_percent, max_pop_delay]
  Result.new(simulation_results, delays, in_system, time_population, percentages)
end