class Record
  def initialize(caller_no, interarrival_time, arrival_time, when_able_avail, when_baker_avail, server_chosen,
                 service_time, service_begins, able_svc_completes, baker_svc_completes, caller_delay, time_in_sys)
    @caller_no = caller_no
    @interarrival_time = interarrival_time
    @arrival_time = arrival_time
    @when_able_avail = when_able_avail
    @when_baker_avail = when_baker_avail
    @server_chosen = server_chosen
    @service_time = service_time
    @service_begins = service_begins
    @able_svc_completes = able_svc_completes
    @baker_svc_completes = baker_svc_completes
    @caller_delay = caller_delay
    @time_in_sys = time_in_sys
  end
  attr_accessor :caller_no, :interarrival_time, :arrival_time, :when_able_avail, :when_baker_avail, :server_chosen, :service_time,
      :service_begins, :able_svc_completes, :baker_svc_completes, :caller_delay, :time_in_sys

end